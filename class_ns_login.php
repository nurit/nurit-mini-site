<?php
/**
 * Created by PhpStorm.
 * User: Nurit
 * Date: 06/08/2018
 * Time: 01:19
 */

require_once ('interface_ins_login.php');

class ns_login implements ins_login {
	/** @var ns_users */
	private  $users;

	/**
	 * ns_login constructor.
	 * @param ns_users $users
	 */
	public function __construct($users) {
		$this->users = $users;
	}

	/**
	 * @return bool
	 */
	public function process(){
		$user_name = empty($_POST['user_name'])?'':$_POST['user_name'];
		$user_ip = $_SERVER['REMOTE_ADDR'];
		$last_logged = time();
		return $this->users->add_user($user_name,$user_ip,$last_logged);
	}

	/**
	 * @return void
	 */
	public function display(){
	    $this->users->inflate();
		ob_start();
		$this->html_wrap_open();
		$this->greeting();
		$this->disconnect();
		$this->users_table_wrapped();
		$this->html_wrap_close();
		ob_get_flush();

	}

	public function users_table(){
			$users = $this->users->get_users(true);
			if (count($users) == 0) {
				?>
                <div class="alert alert-warning">No users to display</div>
				<?php
			} else {
				?>
                <table class="ns_users_table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>IP</th>
                        <th>Last Logged</th>
                        <th>Updated</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
					foreach ($users as $key => $user) {
						?>
                        <tr>
                            <td><?php echo $key; ?></td>
                            <td><?php echo $user['ip']; ?></td>
                            <td><?php echo date('d-m-Y H:i:s', $user['last_logged']); ?></td>
                            <td><?php echo date('d-m-Y H:i:s', $user['updated']); ?></td>
                            <td><?php echo $user['status']; ?></td>
                        </tr>
						<?php
					}
					?>
                    </tbody>
                </table>
				<?php
			}
	}

	private function users_table_wrapped(){
	    ?>
		<div class="ns_users_table_wrap container">
        <?php
        $this->users_table();
        ?>
        </div>
        <?php
    }

	private function html_wrap_open(){
	    ?>
        <html>
        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Login</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="./ns_commands.js"></script>
            <link rel="stylesheet" href="style.css">
        </head>
        <body>
		<?php
    }

	private function html_wrap_close() {
	    ?>
        </body>
	    <?php
	}

	private function greeting(){
		$current = $this->users->get_current_user();
        echo '<div class="ns_greeting container"><div class="col-md-6">Welcome ' . $current . '</div></div>';
	}

	private function disconnect(){
		$current = $this->users->get_current_user();
		?>
        <div class="ns_disconnect_wrap container">
        <button id="ns_disconnect" onclick="ns_disconnect_user(this)" value="<?php echo $current;?>">Disconnect</button>
        </div>
        <?php
	}

}