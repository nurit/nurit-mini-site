<?php
/**
 * Created by PhpStorm.
 * User: Nurit
 * Date: 06/08/2018
 * Time: 03:36
 */

require_once ("class_ns_users.php");
require_once ("class_ns_login.php");

if(!empty($_POST['disconnect'])){
	$user_name = $_POST['disconnect'];
	$usrs = new ns_users();
	$usrs->inflate();
	$result = $usrs->update_user_status($user_name);
	if($result){
		header('Content-Type: application/json');
		echo json_encode($user_name . ' disconnected');
	} else {
		header('HTTP/1.1 500 Internal Server thing');
		header('Content-Type: application/json; charset=UTF-8');
		die(json_encode(array('message' => 'unable to disconnect', 'code' => 666)));
	}

}

if(!empty($_POST['refresh'])){
	ob_start();
	$usrs = new ns_users();
	$login = new ns_login($usrs);
	$login->users_table();
	ob_get_flush();
}