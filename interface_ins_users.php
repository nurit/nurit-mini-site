<?php
/**
 * Created by PhpStorm.
 * User: Nurit
 * Date: 06/08/2018
 * Time: 01:50
 */

interface ins_users {

	/**
	 * deserialize users data
	 *
	 * @return bool
	 */
	public function inflate();

	/**
	 * serialize users data
	 *
	 * @return bool
	 */
	public function deflate();

	/**
	 * get users array
	 *
	 * @param bool $filter
	 * @return array
	 */
	public function get_users($filter=true);

	/**
	 * get name of the user that established the current session
	 *
	 * @return string
	 */
	public function get_current_user();

	/**
	 * add or update a user
	 *
	 * @param string $name
	 * @param string $ip_addr
	 * @param int $last_logged
	 * @param string $status
	 * @return bool
	 */
	public function add_user($name,$ip_addr,$last_logged,$status='active');

	/**
	 * change user status
	 *
	 * @param string $name
	 * @param string $new_status
	 * @return bool
	 */
	public function update_user_status($name, $new_status = "disconnect");
}