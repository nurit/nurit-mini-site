<?php
/**
 * Created by PhpStorm.
 * User: Nurit
 * Date: 05/08/2018
 * Time: 19:23
 */

require_once ('interface_ins_users.php');

class ns_users implements ins_users {
	const USERS_FILE_NAME = 'ns_users_db';
	private $users;
	private $current_user;
//	private $users_fp;

	public function __construct() {
		$fp = fopen('./'.self::USERS_FILE_NAME, 'c');
		if($fp === false){
			$this->users = array();
//			die('failed to create log: ' . self::USERS_FILE_NAME);
		} else {
			fclose($fp);
			$this->inflate();
		}
	}

	private function get_fp($mode='c+'){
		return fopen('./'.self::USERS_FILE_NAME, $mode);
	}

	/**
	 * @return bool
	 */
	function deflate(){
		$fp = $this->get_fp('w');
		$record = json_encode($this->users);
		$result = fputs($fp, $record);
		if($result===false)
			return false;
		fflush($fp);
		return true;
	}

	/**
	 * @return bool
	 */
	function inflate(){
		$fp = $this->get_fp();
		$record = fgets($fp);
		if($record===false) {
			return false;
		}
		$this->users = json_decode($record,true);
		return true;
	}

	/**
	 * @param $filter
	 * @return array
	 */
	function get_users($filter=true){
		if($filter && isset($this->users)){
			$filter_by = 'active';
			return array_filter($this->users, function($var) use ($filter_by){
				return $var['status'] == $filter_by;
			});
		}
		return $this->users;
	}

	/*
	 * @return string
	 */
	public function get_current_user(){
		return $this->current_user;
	}

	/**
	 * @param string $name
	 * @param string $ip_addr
	 * @param int $last_logged
	 * @param string $status
	 * @return bool
	 */
	function add_user($name,$ip_addr,$last_logged,$status='active'){
		if(empty($this->users[$name])){
			$this->users[$name] = array('name'=>$name,'ip'=>$ip_addr,'last_logged'=>$last_logged,'updated'=>time(),'status'=>$status);
		} else {
			$this->users[$name]['ip'] = $ip_addr;
			$this->users[$name]['last_logged'] = $last_logged;
			$this->users[$name]['updated'] = time();
			$this->users[$name]['status'] = $status;
		}

		$this->current_user = $name;

		return $this->deflate();
	}

	/**
	 * @param string $name
	 * @param string $new_status
	 * @return bool
	 */
	public function update_user_status($name, $new_status="disconnect"){
		$this->inflate();
		if(!isset($this->users[$name])) {
			return false;
		}
		$this->users[$name]['status'] = $new_status;
		$this->deflate();
		return true;
	}
}