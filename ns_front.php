<?php
/**
 * Created by PhpStorm.
 * User: Nurit
 * Date: 05/08/2018
 * Time: 20:38
 */

include('class_ns_users.php');
include('class_ns_login.php');

$usrs = new ns_users();
$usrs->inflate();

$frm = new ns_login($usrs);

$frm->process();
$frm->display();

//TODO: mark user as inactive if closes browser