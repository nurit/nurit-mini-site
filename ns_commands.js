jQuery('document').ready(function(){

    setInterval(table_refresh, 3000);

    function table_refresh(){
        var request = jQuery.ajax({
            url: "./ns_ajax_handler.php",
            type: 'post',
            data: {
                refresh: 1,
            }
        });

        request.done(function (response, text_status, jqXHR) {
            jQuery('.ns_users_table_wrap').html(response);
        });

        request.fail(function (jqXHR, textStatus, errorThrown) {
            console.error('error: ' + textStatus, errorThrown);
        });

        request.always(function () {});
    }

    window.addEventListener("beforeunload", function (event) {
        ns_disconnect_display(this,false);
    });
});

function ns_disconnect_user(obj) {
    ns_disconnect_display(obj,true);
}

function ns_disconnect_display(obj,display) {
    var request = jQuery.ajax({
        url: "./ns_ajax_handler.php",
        type: 'post',
        dataType: 'json',
        data: {
            disconnect: jQuery('#ns_disconnect').val(),
        }
    });

    request.done(function (response, text_status, jqXHR) {
        if(display) {
            jQuery('.ns_disconnect_wrap').html('<div class="alert alert-success">' + response + '</div>');
        }
    });

    request.fail(function (jqXHR, textStatus, errorThrown) {
        console.error('error: ' + textStatus + ': ' + jqXHR.responseText, errorThrown);
        if(display) {
            jQuery('.ns_disconnect_wrap').html('<div class="alert alert-danger">' + jQuery.parseJSON(jqXHR.responseText).message + '</div>');
        }
    });

    request.always(function () {
        console.log("always");
    });
}

