<?php
/**
 * Created by PhpStorm.
 * User: Nurit
 * Date: 06/08/2018
 * Time: 01:54
 */

interface ins_login {

	/**
	 * @return bool
	 */
	public function process();

	/**
	 * @return void
	 */
	public function display();

	/**
	 * @return void
	 */
	public function users_table();

}